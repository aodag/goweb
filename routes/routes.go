package routes

import ()

type Route struct {
	Name    string
	Pattern string
}

type Router struct {
	Routes []Route
}

func (router *Router) AddRoute(name string, pattern string) {
	router.Routes = append(router.Routes, Route{Name: name, Pattern: pattern})
}
