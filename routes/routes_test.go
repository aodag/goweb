package routes

import "testing"

func TestInit(t *testing.T) {
	target := Router{}
	if len(target.Routes) != 0 {
		t.Errorf("routes is 0")
	}
}

func TestAddRoute(t *testing.T) {
	target := Router{}
	target.AddRoute("dummy", "/")
	expected := Route{Name: "dummy", Pattern: "/"}
	if len(target.Routes) != 1 {
		t.Errorf("routes is 1")
	}
	if target.Routes[0] != expected {
		t.Errorf("routes include dummy")
	}
}
